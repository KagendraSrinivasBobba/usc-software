
import java.io.*; 
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.Instant;
import java.time.ZoneId;
import java.lang.Object;
import java.util.stream.IntStream; 


public class ExerciseClasses {
    static final double [] PRICE={20,15,10,25,30};
    static String [] dates= {"12-March-2020","13-March-2020","18-March-2020","19-March-2020","24-March-2020","25-March-2020",
                                "1-April-2020","2-April-2020","7-April-2020","8-April-2020","9-April-2020","10-April-2020",
                                "17-April-2020","18-April-2020","25-April-2020","26-April-2020","2-May-2020","3-May-2020"
    
    };
    static List<String[]> timetable = new ArrayList<>();
    static List<String[]> classBookings = new ArrayList<>();
    
    static int classonecount=0;
    static int classtwocount=0;
    static int classthreecount=0;
    static int classfourcount=0;
    static int classfivecount=0;
    
    //contains exercise, number of students then multiply by pricing
    static List<String[]> incomeReportUpdate = new ArrayList<>();
    
    static List<String[]> countReportStatic = new ArrayList<>();
    static List<String[]> countReportStatic2 = new ArrayList<>();
    //contains the name of the group with highest income
    static String highIncomeClass="";
    
    public String getTimetable(String filename,String[] dates,int flag) throws IOException{
        File file = new File(filename); 
        int line=0;
        String message="";
        System.out.println("..........Exercise classes Timetable.................");
        System.out.println("Date...........Morning....Afternoon......Evening");
        InputStream injar = getClass().getResourceAsStream(filename);
        InputStreamReader injarread = new InputStreamReader(injar);
        
        BufferedReader br = new BufferedReader(injarread); 
        String st; 
        try{
            while ((st = br.readLine()) != null) {
                for(int i=0;i<dates.length;i++){
                    if(i+1==line){
                        st=dates[i]+" "+st;
                    }
                }
                if(flag==1){
                    System.out.println(st);
                }
                
                //add the date string
                String[] splitted=st.split(" ");  
                ExerciseClasses.timetable.add(splitted);
                line+=1;
                
            }
            message="succesfully fetched timetable";
        }catch (IOException e){
               String exc="Exception";     
         
        }finally{
             br.close();
            
        }
            return message;
    }

     public static void check(String exercisefile, String[] dates) throws IOException{
            ExerciseClasses reader=new ExerciseClasses();
            reader.getTimetable(exercisefile, dates,0);
            int breakpoint=0;
            int breaker=0;
            int breakpointer=0;
            int breakpointed=0;
            
            String not="Date not found";
            String not2="Exercise name not found";
            String not3="Class is full book another class";
        
        while(true){
        int breakname=0;
        int breaknoback=0;
        int breakquit=0;
        
            System.out.print("Check timetable by: 1.Date  2.Exercise Name..? ");
            Scanner getInput = new Scanner(System.in);
            int option = getInput.nextInt();
            
          
           
            if(option==1){
                while(true){
                List<String[]> timetabler=ExerciseClasses.timetable;
                System.out.print("Enter the date in the form > 2-April-2020: ");
                String date = getInput.next();
                for(int i=0;i<timetabler.size();i++){
                    for(int j=0;j<timetabler.get(i).length;j++ ){
                        if(timetabler.get(i)[j].equals(date)){
                            not="";
                            System.out.println("These are the classes found for the specified date");
                            System.out.print(date+" "+timetabler.get(i)[j+1]+" "+timetabler.get(i)[j+2]+ " "
                                            +timetabler.get(i)[j+3]+" "+timetabler.get(i)[j+4]+" "+timetabler.get(i)[j+5]+" "
                                            + timetabler.get(i)[j+6]+"\n"
                            
                            );
                            //start of proceed while
                            
                            System.out.print("Proceed to booking[Y/N]? ");
                            String proceed= getInput.next();
                                if(proceed.toLowerCase().equals("n")){
                                    breaker=1;
                                    breakpointer=1;
                                    break;
                                }
                                
                               
                            System.out.print("Enter your name: ");
                            String studentname = getInput.next();
                            System.out.print("Enter the class session to book[Morning/Afternoon/Evening]?: "); 
                            String booked = getInput.next();
                                
                          if(booked.toLowerCase().equals("morning") || booked.toLowerCase().equals("afternoon") || booked.toLowerCase().equals("evening") ){
                              
                                //store the booking info
                              if(booked.toLowerCase().equals("morning")){
                                  String exercisename=timetabler.get(i)[j+1];
                                  String time=timetabler.get(i)[j+2];
                                  int counter=classCounter(exercisename);
                                  if(counter!=4){
                                      System.out.println("You have booked  a "+booked+" "+exercisename+" class scheduled at "+time);
                                      updateIncomeReport(exercisename,counter);
                                      String bookings=studentname+","+date+","+exercisename+","+time;
                                      String[] bookarray=bookings.split(",");
                                      classBookings.add(bookarray);
                                      System.out.print("Book another class[Y/N]? ");
                                      String bookmore= getInput.next();
                                      if(bookmore.toLowerCase().equals("y")){
                                            break;
                                       }else{
                                          breaker=1;
                                          breakpointer=1;
                                          break;
                                      }
                                   
                                
                                       
                                  }else{
                                      System.out.println("Class is full book another class");
                                      breaker=1;
                                     
                                  }
       
                                  
         
                              }else if(booked.toLowerCase().equals("afternoon")){
                                  String exercisename=timetabler.get(i)[j+3];
                                  String time=timetabler.get(i)[j+4];
                                  int counter=classCounter(exercisename);
                                  if(counter!=4){
                                      System.out.println("You have booked  a "+booked+" "+exercisename+" class scheduled at "+time);
                                      String bookings=studentname+","+date+","+exercisename+","+time;
                                      String[] bookarray=bookings.split(",");
                                      classBookings.add(bookarray);
                                      updateIncomeReport(exercisename,counter);
                                       System.out.print("Book another class[Y/N]? ");
                                      String bookmore= getInput.next();
                                         if(bookmore.toLowerCase().equals("y")){
                                            break;
                                       }else{
                                          breaker=1;
                                          breakpointer=1;
                                          break;
                                      }
                                        
                                       
                                  }else{
                                      breaker=1;
                                      System.out.println(not3);
                                  }
       
                                  
         
                              }else if(booked.toLowerCase().equals("evening")){
                                  String exercisename=timetabler.get(i)[j+5];
                                  String time=timetabler.get(i)[j+6];
                                  int counter=classCounter(exercisename);
                                  if(counter!=4){
                                      System.out.println("You have booked  a "+booked+" "+exercisename+" class scheduled at "+time);
                                      String bookings=studentname+","+date+","+exercisename+","+time;
                                      String[] bookarray=bookings.split(",");
                                      classBookings.add(bookarray);
                                       updateIncomeReport(exercisename,counter);
                                      System.out.print("Book another class[Y/N]? ");
                                      String bookmore= getInput.next();
                                         if(bookmore.toLowerCase().equals("y")){
                                            break;
                                       }else{
                                          breaker=1;
                                          breakpointer=1;
                                          break;
                                      }
                                  }else{
                                       System.out.println("Class is full book another class");
                                      breaker=1;
                                      
                                  }
       
                                  
         
                              }
                            
                            
                            }
                         
      
                          breakpoint=1;
                          
                           
                       
                           
                        }
                        
                       
                    }
                 if(breaker==1){
                    break;
                }  
                }
                 if(breaker==1){
                    break;
                }
                System.out.println(not);
                }
                
                
               
            }else{
                while(true){
                System.out.print("Enter the exercise name: ");
                String exercise = getInput.next();
                List<String[]> timetable=ExerciseClasses.timetable;
                for(int i=0;i<timetable.size();i++){
                    for(int j=0;j<timetable.get(i).length;j++ ){
                        if(timetable.get(i)[j].toLowerCase().equals(exercise)){
                           reader.getTimetable(exercisefile, dates,1); 
                          breaker=1;
                          breakpoint=1;
                          not2="";
                          break;
                        }
                       
                    }
                if(breakpoint==1){
                    break;
                }      
                }
                if(breaker==1){
                    break;
                }
                }
                
                System.out.println(not2);
                         //initiate booking
                breakpointer=book(timetable);
        
               
                
                
                
             
                 
            }
            if(breakpointer==1){
                    break;
            }
            
        } 
    }

     public static int classCounter(String classname){
        int full=0;
        if(classname.toLowerCase().equals("yoga")){
            if(ExerciseClasses.classonecount!=4){
                ExerciseClasses.classonecount+=1;
            }else{
                full=4;
            }   
        }else if(classname.toLowerCase().equals("zumba")){
            if(ExerciseClasses.classtwocount!=4){
                ExerciseClasses.classtwocount+=1;
            }else{
                full=4;
            }   
        }else if(classname.toLowerCase().equals("aquacise")){
            if(ExerciseClasses.classthreecount!=4){
                ExerciseClasses.classthreecount+=1;
            }else{
                full=4;
            }   
        }else if(classname.toLowerCase().equals("box-fit")){
            if(ExerciseClasses.classfourcount!=4){
                ExerciseClasses.classfourcount+=1;
            }else{
                full=4;
            }   
        }else if(classname.toLowerCase().equals("body-blitz")){
            if(ExerciseClasses.classonecount!=4){
                ExerciseClasses.classonecount+=1;
            }else{
                full=4;
            }   
        }
        
        return full;
    
    }

     public static int book(List<String[]> timetable2){
        
                int breaker=0;
                int breakpoint=0;
                int breakpointer=0;
           
                String not="Date not found";
                String not2="Session not found";
                String not3="Class is full book another class";
                Scanner getInput = new Scanner(System.in);
                while(true){
                List<String[]> timetabler=ExerciseClasses.timetable;
                System.out.print("Enter the date in the form > 2-April-2020: ");
                String date = getInput.next();
                for(int i=0;i<timetabler.size();i++){
                    for(int j=0;j<timetabler.get(i).length;j++ ){
                        if(timetabler.get(i)[j].equals(date)){
                            not="";
                            System.out.println("These are the classes found for the specified date");
                            System.out.print(date+" "+timetabler.get(i)[j+1]+" "+timetabler.get(i)[j+2]+ " "
                                            +timetabler.get(i)[j+3]+" "+timetabler.get(i)[j+4]+" "+timetabler.get(i)[j+5]+" "
                                            + timetabler.get(i)[j+6]+"\n"
                            
                            );
                            //start of proceed while
                            
                            System.out.print("Proceed to booking[Y/N]? ");
                            String proceed= getInput.next();
                                if(proceed.toLowerCase().equals("n")){
                                    breaker=1;
                                    breakpointer=1;
                                    break;
                                }
                                
                               
                            System.out.print("Enter your name: ");
                            String studentname = getInput.next();
                            System.out.print("Enter the class session to book[Morning/Afternoon/Evening]?: "); 
                            String booked = getInput.next();
                                
                          if(booked.toLowerCase().equals("morning") || booked.toLowerCase().equals("afternoon") || booked.toLowerCase().equals("evening") ){
                               
                                //store the booking info
                              if(booked.toLowerCase().equals("morning")){
                                  String exercisename=timetabler.get(i)[j+1];
                                  String time=timetabler.get(i)[j+2];
                                  int counter=classCounter(exercisename);
                                  if(counter!=4){
                                      System.out.println("You have booked  a "+booked+" "+exercisename+" class scheduled at "+time);
                                      String bookings=studentname+","+date+","+exercisename+","+time;
                                      String[] bookarray=bookings.split(",");
                                      classBookings.add(bookarray);
                                      updateIncomeReport(exercisename,counter);
                                      System.out.print("Book another class[Y/N]? ");
                                      String bookmore= getInput.next();
                                      if(bookmore.toLowerCase().equals("y")){
                                            break;
                                       }else{
                                          breaker=1;
                                          breakpointer=1;
                                          break;
                                      }
                                   
                                
                                       
                                  }else{
                                      System.out.println("Class is full book another class");
                                      breaker=1;
                                      
                                  }
       
                                  
         
                              }else if(booked.toLowerCase().equals("afternoon")){
                                  String exercisename=timetabler.get(i)[j+3];
                                  String time=timetabler.get(i)[j+4];
                                  int counter=classCounter(exercisename);
                                  if(counter!=4){
                                      System.out.println("You have booked  a "+booked+" "+exercisename+" class scheduled at "+time);
                                      String bookings=studentname+","+date+","+exercisename+","+time;
                                      String[] bookarray=bookings.split(",");
                                      classBookings.add(bookarray);
                                      updateIncomeReport(exercisename,counter);
                                       System.out.print("Book another class[Y/N]? ");
                                      String bookmore= getInput.next();
                                         if(bookmore.toLowerCase().equals("y")){
                                            break;
                                       }else{
                                          breaker=1;
                                          breakpointer=1;
                                          break;
                                      }
                                        
                                       
                                  }else{
                                      System.out.println("Class is full book another class");
                                      breaker=1;
                                      
                                  }
       
                                  
         
                              }else if(booked.toLowerCase().equals("evening")){
                                  String exercisename=timetabler.get(i)[j+5];
                                  String time=timetabler.get(i)[j+6];
                                  int counter=classCounter(exercisename);
                                  if(counter!=4){
                                      System.out.println("You have booked  a "+booked+" "+exercisename+" class scheduled at "+time);
                                      String bookings=studentname+","+date+","+exercisename+","+time;
                                      String[] bookarray=bookings.split(",");
                                      classBookings.add(bookarray);
                                       updateIncomeReport(exercisename,counter);
                                      System.out.print("Book another class[Y/N]? ");
                                      String bookmore= getInput.next();
                                         if(bookmore.toLowerCase().equals("y")){
                                            break;
                                       }else{
                                          breaker=1;
                                          breakpointer=1;
                                          break;
                                      }
                                  }else{
                                      System.out.println("Class is full book another class");
                                      breaker=1;
                                      
                                  }
       
                                  
         
                              }
                            
                            
                            }
                         
      
                          breakpoint=1;
                          
                           
                       
                           
                        }
                        
                       
                    }
                 if(breaker==1){
                    break;
                }  
                }
                 if(breaker==1){
                    break;
                }
                System.out.println(not);
                }
                return breakpointer;
    }

    public static void updateIncomeReport(String classname, int studentcount){
        String newstring=classname+","+String.valueOf(studentcount);
        String[] newarray=newstring.split(newstring);
        String classnamelarge="";
        int[] prices=new int[10];
        
        for(int i=0;i<incomeReportUpdate.size();i++){
                    for(int j=0;j<incomeReportUpdate.get(i).length;j++ ){
                        if(incomeReportUpdate.get(i)[j].equals(classname)){
                            incomeReportUpdate.set(i,newarray);
                        }
                    }
        }
        
        for(int i=0;i<incomeReportUpdate.size();i++){
                    for(int j=0;j<incomeReportUpdate.get(i).length;j++ ){
                        if(incomeReportUpdate.get(i)[j].toLowerCase().equals("yoga")){
                            prices[0]=50*Integer.parseInt(incomeReportUpdate.get(i)[j+1]);
                        }else if(incomeReportUpdate.get(i)[j].toLowerCase().equals("zumba")){
                            prices[1]=80*Integer.parseInt(incomeReportUpdate.get(i)[j+1]);
                        }else if(incomeReportUpdate.get(i)[j].toLowerCase().equals("aquacise")){
                            prices[2]=110*Integer.parseInt(incomeReportUpdate.get(i)[j+1]);
                        }else if(incomeReportUpdate.get(i)[j].toLowerCase().equals("box-fit")){
                            prices[3]=140*Integer.parseInt(incomeReportUpdate.get(i)[j+1]);
                        }else if(incomeReportUpdate.get(i)[j].toLowerCase().equals("body-blitz")){
                            prices[4]=170*Integer.parseInt(incomeReportUpdate.get(i)[j+1]);
                        }
                    }
        }
        
        int max = prices[0];
        for(int i = 1; i < prices.length;i++){
            if(prices[i] > max){
                max = prices[i];
            }
  }
        
        for(int i = 0; i < prices.length;i++){
            if(prices[i]==max){
                if(i==0){
                   classnamelarge="Yoga"; 
                }else if(i==1){
                   classnamelarge="Zumba"; 
                }else if(i==2){
                   classnamelarge="Aquacise"; 
                }else if(i==3){
                   classnamelarge="Box-Fit"; 
                }else if(i==4){
                   classnamelarge="Body-Blitz"; 
                }
            }
        }
        
        highIncomeClass=classnamelarge;
    }



 public static void updateRatingPerDay(String date,String classname,int rating) {
        String report=date+","+classname+","+String.valueOf(rating);
        String[] reportarray=report.split(",");
        countReportStatic.add(reportarray);  
    }
    
    public static void updateStudentPerDay(String date,String classname,int studentcount) {
        String report=date+","+classname+","+String.valueOf(studentcount);
        String[] reportarray=report.split(",");
        countReportStatic2.add(reportarray);  
    }

    public static void reports() throws ParseException{
    //exercise start dates
        String startingdate=dates[0];
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMMM-yyyy", Locale.US);
        Date parsed=formatter.parse(startingdate);
      
        //current date
        Date date=new java.util.Date();  
        System.out.println(); 
        ZoneId defaultZoneId = ZoneId.systemDefault();
        //exercise start date converted to localdate
        Instant instantstart = parsed.toInstant();
        LocalDate localparsed=instantstart.atZone(defaultZoneId).toLocalDate();
        //exercise current date converted to localdate
        Instant instantend = date.toInstant();
        LocalDate localdate=instantend.atZone(defaultZoneId).toLocalDate();
        //getting the difference between the dates
        //if date is greater than 8 days/4 weekends, the report is generated
        Period daysdifference = Period.between(localparsed, localdate);   

        if(daysdifference.getDays()>8){
            System.out.println("From the bookings data the class with the"
                    + " highest income is "+highIncomeClass);
            //show the day,class,number of students,and average rating
            for(int i=0;i<ExerciseClasses.countReportStatic2.size();i++){
                    for(int j=0;j<ExerciseClasses.countReportStatic2.get(i).length;j++ ){
                        for(int k=0;k<ExerciseClasses.incomeReportUpdate.size();k++){
                             for(int l=0;l<ExerciseClasses.incomeReportUpdate.get(k).length;l++ ){
                                 if(ExerciseClasses.countReportStatic2.get(i)[j+1].toLowerCase().equals(ExerciseClasses.incomeReportUpdate.get(k)[l].toLowerCase())){
                                   System.out.println("Day: "+ExerciseClasses.countReportStatic2.get(i)[j]
                                   +" Exercise: "+ExerciseClasses.countReportStatic2.get(i)[j+1] + " Student count: "+ExerciseClasses.incomeReportUpdate.get(k)[l+1]+
                                           " rating: "+ExerciseClasses.countReportStatic2.get(i)[j+3]);
                                 }
                        }
                }
                    }
            }
            
        }
    }

    public static void run() throws IOException,ParseException{
        System.out.println("........USC Group Exercise Bookings Software.........");
        System.out.println("Checking for reports......");
        
        System.out.println(".............................");
        Scanner getInput = new Scanner(System.in);
        while (true){
            System.out.println("1. Book a class");
            System.out.println("2. Change a booking");
            System.out.println("3. Write a review");
            System.out.print("Choose an option to continue: ");
            String option = getInput.next();  
            
            if(option.equals("1")){
                check("exercise.txt",dates);
            }else if(option.equals("2")){
                String noc="";
                System.out.print("Enter your name: ");
                String name = getInput.next(); 
                for(int i=0;i<ExerciseClasses.classBookings.size();i++){
                    for(int j=0;j<ExerciseClasses.classBookings.get(i).length;j++ ){
                        if(ExerciseClasses.classBookings.get(i)[j].equals(name)){
                            System.out.println("You have a "+ExerciseClasses.classBookings.get(i)[j+2]+" class booking for "+ExerciseClasses.classBookings.get(i)[j+1]+" at "
                                    +ExerciseClasses.classBookings.get(i)[j+3]);
                            System.out.print("Change this booking?[Y/N]: ");
                            String change= getInput.next();
                            if(change.toLowerCase().equals("y")){
                                check("exercise.txt",dates);
                                noc="Booking succesfully changed";
                            }else{
                                noc="Booking not changed";
                            }
                        }
                    }
            }
                System.out.print(noc);
        }else if(option.equals("3")){
                String noc="";
                String not="Booking not found";
                System.out.print("Enter your name: ");
                String name = getInput.next(); 
                int r=0;
                for(int i=0;i<ExerciseClasses.classBookings.size();i++){
                    for(int j=0;j<ExerciseClasses.classBookings.get(i).length;j++ ){
                        if(ExerciseClasses.classBookings.get(i)[j].equals(name)){
                            not="";
                            System.out.println("You attended a "+ExerciseClasses.classBookings.get(i)[j+2]+" class booking on "+ExerciseClasses.classBookings.get(i)[j+1]+" at "
                                    +ExerciseClasses.classBookings.get(i)[j+3]);
                            System.out.print("Review this class?[Y/N]: ");
                            String change= getInput.next();
                            if(change.toLowerCase().equals("y")){
                                
                                r=1;
                            }else{
                                noc="Class not reviewed";
                            }
                            if(r==1){
                                System.out.println("####..Showing class ratings...###");
                                System.out.println("1. Very dissatisfied");
                                System.out.println("2. Dissatisfied");
                                System.out.println("3. Ok");
                                System.out.println("4. Satisfied");
                                System.out.println("5. Very Satisfied");
                                System.out.print("Choose a rating from your class experience: ");
                                String rating = getInput.next();
                                System.out.print("Enter your review: ");
                                String review = getInput.next();
                                String dayclassnamereviewrating=ExerciseClasses.classBookings.get(i)[j+2]+","+ExerciseClasses.classBookings.get(i)[j+1]+","+rating;
                                String[] ratings=dayclassnamereviewrating.split(",");
                                countReportStatic2.add(ratings);
                                System.out.println("You have succesfully rated the class");
                                ExerciseClasses.reports();
                            }
                            
                        }
                    }
            }
                System.out.println(noc);
                System.out.println(not);
        }
        }
    }








   

    public static void main(String[] args) throws IOException,ParseException {
      run()
      
       
    }
    
}
